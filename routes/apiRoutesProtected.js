var express            = require('express');
var apiRoutesProtected = express.Router();
var jwt                = require('jsonwebtoken');
var config             = require('../config');
var User               = require('../models/user');
var Vehicle            = require('../models/vehicle');
var Trackbox           = require('../models/trackbox');
var Accreading         = require('../models/accreading');
var Gpsreading         = require('../models/gpsreading');
var Gyroreading        = require('../models/gyroreading');
var knex               = require('../models/db').knex;

// route middleware to verify a token
apiRoutesProtected.use(function(req, res, next){
    // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, config.secret, function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' , error: err});    
      } else {
        // if everything is good, save to request for use in other routes
        User.where({'user_email': decoded.email}).fetch().then(function(user){
            req.decoded = decoded;
            req.user = user;
            req.email = decoded.email;
            req.token = token;
            next();
        });
      }
    });

  } else {
    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});

apiRoutesProtected.get('/vehicles', function(req, res){
    req.user.fetch({withRelated: ['vehicles']}).then(function(user){
        if(!user){
            return res.json({success: false, message: 'Cannot retreive vehicles'});
        }
        var vehicles = user.related('vehicles');
        //return res.json({success: true, message: vehicles});
        return res.render('vehicles', {title: 'Vehicles', 'vehicles': vehicles.toJSON(), active:'vehicles_nav_button', token: req.token});
    });
});

apiRoutesProtected.get('/trackboxes', function(req, res){
    req.user.fetch({withRelated: ['trackboxes']}).then(function(user){
        if(!user){
            return res.json({success: false, message: 'Cannot retreive trackboxes'});
        }
        var trackboxes = user.related('trackboxes');
        res.render('trackboxes', {title: 'Trackboxes', 'trackboxes': trackboxes.toJSON(),  active:'trackboxes_nav_button', token: req.token});
    });
});

apiRoutesProtected.post('/addVehicle', function(req, res){
    if(!req.body.name){
       return res.json({success: false, message: 'Please fill out at least the name of the vehicle'});
    }else{
        if(req.body.model){
            var atts = {'vehicle_name': req.body.name.toLowerCase(), 'vehicle_model': req.body.model, 'user_id_fk': req.user.id};
        }else{
            var atts = {'vehicle_name': req.body.name, 'user_id_fk': req.user.id};
        }
        Vehicle.where(atts).fetch().done(function(user){
            if(user)return res.json({success: false, message: 'Vehicle already added'});
            else{
                if(!req.body.model){
                    new Vehicle({
                        'vehicle_name': req.body.name.toLowerCase(), 
                        'user_id_fk': req.user.id}).save().then(function(){
                            return res.json({success: true, message: 'Vehicle saved'});
                     });
                }else{
                    new Vehicle({
                        'vehicle_name': req.body.name.toLowerCase(),
                        'vehicle_model': req.body.model,
                        'user_id_fk': req.user.id}).save().then(function(){
                            return res.json({success: true, message: 'Vehicle saved'});
                     });
                }
            }
        });
    } 

});

apiRoutesProtected.post('/addTrackbox', function(req, res){
    new Trackbox({
            'user_id_fk': req.user.id
    }).save().done(function(trackbox){
       return res.json({success: true, message: "Trackbox added", trackbox_id: trackbox.id});
    });
});

apiRoutesProtected.post('/Vehicle/addTrackbox', function(req, res){
    if(!req.body.trackbox) return res.json({success: false, message: 'Invalid trackbox id'});
    if(!req.body.vehicle) return res.json({success: false, message: 'Invalid vehicle id'});
    Vehicle.where({'trackbox_id_fk': req.body.trackbox}).fetch().done(function(vehicle){
        if(vehicle) return res.json({success: false , message: 'Trackbox '+ req.body.trackbox + ' is already assigned to vehicle '+ vehicle.id});
        Vehicle.where({'vehicle_id': req.body.vehicle}).save({'trackbox_id_fk': req.body.trackbox},{method: 'update'}).done(function(trackbox){
            if(!trackbox) return res.json({success: false, message: "Error assigning trackbox to vehicle"});
            return res.json({success: true, message: 'Trackbox ' + req.body.trackbox + ' assigned to vehicle'});
        });
    });
});

apiRoutesProtected.get('/vehicle/accReadings', function(req, res){
    var vehicle_query = req.query.vehicle;
    var farLimit      = req.query.far;
    var nearLimit     = req.query.near;
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
       if(farLimit && nearLimit){
           Accreading.where({'vehicle_id_fk': vehicle_query}).where('timestamp', '>', nearLimit).where('timestamp', '<', farLimit).fetchAll().then(function(readings){
               if(!readings) return res.json({success: false, message: 'Cannot retreive Accelerometer readings'});
               return res.json({success: true, message: readings});
           });
       }
       else{
           Accreading.where({'vehicle_id_fk': vehicle_query}).fetchAll().then(function(readings){
               if(!readings) return res.json({success: false, message: 'Cannot retreive Accelerometer readings'});
               return res.json({success: true, message: readings});
           });

       }
    });
});

apiRoutesProtected.get('/vehicle/gyroReadings', function(req, res){
    var vehicle_query = req.query.vehicle;
    var farLimit      = req.query.far;
    var nearLimit     = req.query.near;
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
       if(farLimit && nearLimit){
           Gyroreading.where({'vehicle_id_fk': vehicle_query}).where('timestamp', '>', nearLimit).where('timestamp', '<', farLimit).fetchAll().then(function(readings){
               if(!readings) return res.json({success: false, message: 'Cannot retreive Accelerometer readings'});
               return res.json({success: true, message: readings});
           });
       }else{
           Gyroreading.where({'vehicle_id_fk': vehicle_query}).fetchAll().then(function(readings){
               if(!readings) return res.json({success: false, message: 'Cannot retreive Accelerometer readings'});
               return res.json({success: true, message: readings});
           });
       }
    });
});

apiRoutesProtected.get('/vehicle/gpsReadings', function(req, res){
    var vehicle_query = req.query.vehicle;
    var farLimit      = req.query.far;
    var nearLimit     = req.query.near;
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
       if(nearLimit && farLimit){
           Gpsreading.where({'vehicle_id_fk': vehicle_query}).where('timestamp', '>', nearLimit).where('timestamp', '<', farLimit).fetchAll().then(function(readings){
               if(!readings) return res.json({success: false, message: 'Cannot retreive Accelerometer readings'});
               return res.json({success: true, message: readings});
           });
       }else{
           Gpsreading.where({'vehicle_id_fk': vehicle_query}).fetchAll().then(function(readings){
               if(!readings) return res.json({success: false, message: 'Cannot retreive Accelerometer readings'});
               return res.json({success: true, message: readings});
           });
       }
    });
});

/*
* Readings are posted to the following routes in the form of a JSON file
* in the following formar:
* {
*      "vehicle": <vehicle_id>, //this is the vehicle id where the readings come from
*      "readings": [{"x": <x-reading>, "y": y-reading, "z": z-reading, "timestamp": <timestamp>}, .........]
*/
apiRoutesProtected.post('/vehicle/accReadings', function(req, res){
    var vehicle_query = req.body['vehicle'];
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
       var values = addVehileId(req.body['readings'], vehicle_query);
       if(values == 0) return res.json({success: true, message: 'no new values added was empty for accReadings of vehicle with id ' + vehicle_query});
       knex.insert(values, 'vehicle_id_fk').into('accreading').then(function(a){
           return res.json({success: true, message: 'Accelerometer readings inserted successfully for vehicle with id ' + vehicle_query});
       });
    });
});

apiRoutesProtected.post('/vehicle/gyroReadings', function(req, res){
    var vehicle_query = req.body['vehicle'];
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
       var values = addVehileId(req.body['readings'], vehicle_query);
       if(values.length == 0) return res.json({success: true, message: 'no new values added was empty for gyroReadings of vehicle with id ' + vehicle_query});
       knex.insert(values, 'vehicle_id_fk').into('gyroreading').then(function(a){
           return res.json({success: true, message: 'Gyroscope readings inserted successfully for vehicle with id ' + vehicle_query});
       });
    });
});

apiRoutesProtected.post('/vehicle/GPSReadings', function(req, res){
    var vehicle_query = req.body['vehicle'];
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
       var values = addVehileId(req.body['readings'], vehicle_query);
       if(values.length == 0) return res.json({success: true, message: 'no new values added was empty for gpsReadings of vehicle with id ' + vehicle_query});
       knex.insert(values, 'vehicle_id_fk').into('gpsreading').then(function(a){
           return res.json({success: true, message: 'GPS readings inserted successfully for vehicle with id ' + vehicle_query});
       });
    });
});

var addVehileId = function(readingsArray, vehicle_query){
    for(var i = 0; i< readingsArray.length; i++){
        readingsArray[i]['vehicle_id_fk'] = vehicle_query;
    }
    return readingsArray;
}

apiRoutesProtected.get('/vehicle/readings', function(req, res){
    var vehicle_query = req.query.vehicle;
    if(!vehicle_query) return res.json({success: false, message: 'Vehicle must be choosen'});
    var user_id = req.user.id;
    Vehicle.where({'user_id_fk': user_id, 'vehicle_id': vehicle_query}).fetch().done(function(vehicle){
       if(!vehicle) return res.json({success: false, message: 'Vehicle' + ' ' + vehicle + ' doesn\'t belong to this user'}); 
        var data = {title: 'Readings',
                    token: req.token,
                    vehicle: req.query.vehicle,
                    name: vehicle.toJSON().vehicle_name,
                    model: vehicle.toJSON().vehicle_model,
                    trackbox: vehicle.toJSON().trackbox_id_fk,
                    active: 'vehicles_nav_button'
        }
        res.render('readings/readings', data);
    });
});

module.exports = apiRoutesProtected;
