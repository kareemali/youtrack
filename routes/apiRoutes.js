var express   = require('express');
var apiRoutes = express.Router();
var User      = require('../models/user');
var bcrypt    = require('bcrypt-nodejs');
//var flash     = require('connect-flash');

apiRoutes.get('/', function(req, res){
    res.render('home', {title: 'YouTrack', token: req.query['token']});
});


apiRoutes.get('/authenticate', function(req, res){
    res.render('authenticate', {title: 'Login'});
});

apiRoutes.post('/authenticate', function(req, res){
    if(!req.body.email || !req.body.password){
        //return res.json({success: false, message: 'Please fill out all fields'});
        return res.render('authenticate', {title: 'Login', error_flash: 'Please fill out all fields'});
    }
    User.where({'user_email': req.body.email}).fetch().done(function(user){
        if(!user){
            if(req.body.mobile) return res.json({success: false, message: 'Incorrect Email'});
            return res.render('authenticate', {title: 'Login', error_flash: 'Incorrect Email'});
        }
        else{
            bcrypt.compare(req.body.password,user.attributes.user_password,function(err, bcryptRes){
                if(err) return res.json({success: false, message: 'Bcrypt Error'});
                if(!bcryptRes) return res.json({success: false, message: 'Wrong password'});
                if(req.body.mobile) return res.json({success: true, message: 'Authenticated and token sent', token: user.generateJWT()});
                return res.render('home', {title: 'YouTrack', token: user.generateJWT(), success_flash: 'Signed in'});
            });
        }
    });
});

apiRoutes.get('/register', function(req, res){
    res.render('register', {title: 'Register'});
});

apiRoutes.post('/register', function(req, res){
    if(!req.body.email || !req.body.password){
        return res.json({success: false, message: 'Please fill out all fields'});
    }
    User.where({'user_email': req.body.email.toLowerCase()}).fetch().done(function(user){
        if(user) return res.json({success: false, message: 'Email already exists'});
        User.createPassword(req.body.password,function(hash){
            return new User({
                user_email: req.body.email.toLowerCase(),
                   user_password: hash
            }).save().then(function(){
                //return res.json({success: true, message: 'New user saved'});
                res.render('home', {title: 'YouTrack'});
                
            });
        });
    });
});


apiRoutes.get('/logout', function(req, res){
    res.render('home', {title: 'YouTrack', success_flash: 'loged out'});
});

module.exports = apiRoutes;
