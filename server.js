var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var morgan     = require('morgan');
var jwt        = require('jsonwebtoken');
//var flash      = require('connect-flash');

//require the routes of the api
var apiRoutes          = require('./routes/apiRoutes');
var apiRoutesProtected = require('./routes/apiRoutesProtected');
var User               = require('./models/user');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
//app.use(flash());
//app.use(function(req, res, next){
    //res.locals.success = req.flash('success');
    //res.locals.errors = req.flash('error');
    //next();
//});

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/bower_components'));
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.set('view engine', 'jade');

app.use(morgan('dev'));

app.use('/', apiRoutes);
app.use('/', apiRoutesProtected);

var port = process.env.PORT || 3000;
app.listen(port);
console.log('Magic happens at http://localhost:' + port);
