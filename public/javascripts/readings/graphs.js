$(function() {
    $('#speedgraph-tpick1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
    $('#speedgraph-tpick2').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
});

function drawSpeedGraph(lineData){
    var vis = d3.select('#speed-vis'),
        WIDTH = 1000,
        HEIGHT = 500,
        MARGINS = {
          top: 20,
          right: 20,
          bottom: 20,
          left: 50
        },
        timeFormat = d3.time.format.iso,
        xRange = d3.time.scale()
            .range([MARGINS.left, WIDTH - MARGINS.right])
            .domain(d3.extent(lineData, function(d){
                return timeFormat.parse(d.timestamp);
            })),
            //.domain([d3.min(lineData, function(d) {
                  //return d.timestamp;
                //}), d3.max(lineData, function(d) {
                  //return d.timestamp;
                //})]),
        yRange = d3.scale.linear()
            .range([HEIGHT - MARGINS.top, MARGINS.bottom])
            .domain([d3.min(lineData, function(d) {
                  return d.speed;
                }), d3.max(lineData, function(d) {
                  return d.speed;
                })]),
        xAxis = d3.svg.axis()
          .scale(xRange)
          .tickSize(2)
          .tickSubdivide(true),
        yAxis = d3.svg.axis()
          .scale(yRange)
          .tickSize(2)
          .orient('left')
          .tickSubdivide(true);

    vis.append('svg:g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
      .call(xAxis);

    vis.append('svg:g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
      .call(yAxis);

    d3.selectAll('text').attr('stroke', '#7157B1');


    var lineFunc = d3.svg.line()
      .x(function(d) {
        return xRange(timeFormat.parse(d.timestamp));
      })
      .y(function(d) {
        return yRange(d.speed);
      })
      .interpolate('linear');

    vis.append('svg:path')
      .attr('d', lineFunc(lineData))
      .attr('stroke', 'blue')
      .attr('stroke-width', 2)
      .attr('fill', '#FFFFFF');
}

$('#speed-graph-options').submit(function(event){
    event.preventDefault();
    var token = $('#token').text();
    var vehicle = $('#vehicle-id').text();
    var nearDate = $('#speedgraph-tpick1').data().date;
    var farDate = $('#speedgraph-tpick2').data().date;
    $('#speed-vis').remove();
    $('#speed-graph-panel').append('<svg id="speed-vis" width="1000" height="500"></svg>');
    var sensor = 'gpsReadings';
    $.ajax({
        type: 'GET',
        url: '/vehicle/' + sensor,
        data: {'vehicle': vehicle, 'near': nearDate, 'far': farDate},
        headers: {
            'x-access-token': token
        },
        success: function(text){
            var lineData = text.message;
            console.log(lineData);
            drawSpeedGraph(lineData);
    }
    });
});
