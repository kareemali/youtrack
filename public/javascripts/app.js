$(function(){
    $('#error_flash').delay(2000).fadeOut(200);
    $('#success_flash').delay(1500).fadeOut(200);
    var token = $('#token').text();
    if(token != ''){
        $('#trackboxes_nav_button').attr('href', '/trackboxes?token=' + token);
        $('#vehicles_nav_button').attr('href', '/vehicles?token=' + token);
        $('#logo_button').attr('href', '/?token=' + token);
    }
});

$(function() {
    $('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
    $('#datetimepicker2').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
});

$('#readings-options').submit(function(event){
    event.preventDefault();
    var token = $('#token').text();
    var vehicle = $('#vehicle-id').text();
    var sensor = $('#sensor-select').val();
    var nearDate = $('#datetimepicker1').data().date;
    var farDate = $('#datetimepicker2').data().date;
    var isGPS = false;
    switch(sensor){
        case 'Accelerometer':
            sensor = 'accReadings';break;
        case 'GPS':
            sensor = 'gpsReadings';isGPS = true; break;
        case 'Gyroscope':
            sensor = 'gyroReadings';break;
    }
    $.ajax({
        type: 'GET',
        url: '/vehicle/' + sensor,
        data: {'vehicle': vehicle, 'near': nearDate, 'far': farDate},
        headers: {
            'x-access-token': token
        },
        success: function(text){
            var message = text.message;
            $('#readings-table').remove();
            $('#raw').append('<table class="table table-striped table-hover" id="readings-table"></table>'); 
            var table = $('#readings-table');
            if(!isGPS){
                var thead = '<thead><tr><th>x-axis</th><th>y-axis</th><th>z-axis</th><th>timestamp</th></tr></thead>';
                table.append(thead);
                var rows = '';
                for(var i =0; i<text.message.length; i++){
                    var row = '<tbody><tr><td>' + message[i].x + '</td>';
                    row += '<td>' + message[i].y + '</td>';
                    row += '<td>' + message[i].z + '</td>';
                    row += '<td>' + message[i].timestamp + '</td></tr></tbody>';
                    rows += row;
                }
                table.append(rows);
            }else{
                var thead = '<thead><tr><th>Longitude</th><th>Latitude</th><th>Speed</th><th>timestamp</th></tr></thead>';
                table.append(thead);
                var rows ='';
                for(var i =0; i<text.message.length; i++){
                    var row = '<tbody><tr><td>' + message[i].longitude+ '</td>';
                    row += '<td>' + message[i].latitude+ '</td>';
                    row += '<td>' + message[i].speed + '</td>';
                    row += '<td>' + message[i].timestamp + '</td></tr></tbody>';
                    rows += row;
                }
                table.append(rows);
           }
    }
    });
});
