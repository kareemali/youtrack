DROP DATABASE IF EXISTS `youtrack`;
CREATE SCHEMA IF NOT EXISTS `youtrack`;
-- USE `youtrack`;

-- -----------------------------------------------------
-- Table `youtrack`.`user_info`
-- -----------------------------------------------------;
DROP TABLE IF EXISTS `youtrack`.`user_info`;

CREATE TABLE IF NOT EXISTS `youtrack`.`user_info` (
    `user_id` INT(70) NOT NULL AUTO_INCREMENT,
    `user_email` VARCHAR(45) NOT NULL,
    -- `user_password` VARCHAR(60) NULL,
    `user_password` CHAR(60) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
    `user_join_data` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`user_id`), 
    UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `youtrack`.`trackbox`
-- -----------------------------------------------------;
DROP TABLE IF EXISTS `youtrack`.`trackbox`;

CREATE TABLE IF NOT EXISTS `youtrack`.`trackbox` (
    `trackbox_id` INT(70) NOT NULL AUTO_INCREMENT,
    `user_id_fk` INT(70) NOT NULL,
    PRIMARY KEY (`trackbox_id`),
    CONSTRAINT `trackbox_user_foreign_key`
        FOREIGN KEY (`user_id_fk`)
        REFERENCES `youtrack`.`user_info` (`user_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    -- CONSTRAINT `trackbox_vehicle_foreign_key`
        -- FOREIGN KEY (`vehicle_id_fk`)
        -- REFERENCES `youtrack`.`vehicle` (`vehicle_id`)
        -- ON DELETE NO ACTION
        -- ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `youtrack`.`vehicle`
-- -----------------------------------------------------;
DROP TABLE IF EXISTS `youtrack`.`vehicle`;

CREATE TABLE IF NOT EXISTS `youtrack`.`vehicle` (
    `vehicle_id` INT(70) NOT NULL AUTO_INCREMENT,
    `vehicle_model` INT(2) NULL,
    `vehicle_name` VARCHAR(70) NULL,
    `user_id_fk` INT(70) NOT NULL,
    `trackbox_id_fk` INT(70) NULL UNIQUE,
    PRIMARY KEY (`vehicle_id`),
    CONSTRAINT `vehicle_user_foreign_key`
        FOREIGN KEY (`user_id_fk`)
        REFERENCES `youtrack`.`user_info` (`user_id`)
        ON DELETE NO ACTION
        ON UPDATE no ACTION,
    CONSTRAINT `vehicle_trackbox_foreign_key`
        FOREIGN KEY (`trackbox_id_fk`)
        REFERENCES `youtrack`.`trackbox` (`trackbox_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- add foreign key to trackbox
-- ALTER TABLE `youtrack`.`trackbox` ADD
    -- `vehicle_id_fk` INT(70) NULL;
-- ALTER TABLE `youtrack`.`trackbox` ADD 
    -- CONSTRAINT `trackbox_vehicle_foreign_key`
        -- FOREIGN KEY (`vehicle_id_fk`)
        -- REFERENCES `youtrack`.`vehicle` (`vehicle_id`)
        -- ON DELETE NO ACTION
        -- ON UPDATE NO ACTION;

-- ------------------------------------------------------------
-- Table `youtrack`.`accreading` -- for Accelerometer readings 
-- ------------------------------------------------------------;
DROP TABLE IF EXISTS `youtrack`.`accreading`;

CREATE TABLE IF NOT EXISTS `youtrack`.`accreading` (
    `x` DECIMAL(10,5),
    `y` DECIMAL(10,5),
    `z` DECIMAL(10,5),
    `vehicle_id_fk` INT(70) NULL,
    `timestamp` DATETIME NOT NULL,
    CONSTRAINT `acc_vehicle_foreign_key`
        FOREIGN KEY (`vehicle_id_fk`)
        REFERENCES `youtrack`.`vehicle` (`vehicle_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ------------------------------------------------------------
-- Table `youtrack`.`gyroreading` -- for Gyroscope readings 
-- ------------------------------------------------------------;
DROP TABLE IF EXISTS `youtrack`.`gyroreading`;

CREATE TABLE IF NOT EXISTS `youtrack`.`gyroreading`(
    `x` DECIMAL(10,5),
    `y` DECIMAL(10,5),
    `z` DECIMAL(10,5),
    `vehicle_id_fk` INT(70) NULL,
    `timestamp` DATETIME NOT NULL,
    CONSTRAINT `gyro_vehicle_foreign_key`
        FOREIGN KEY (`vehicle_id_fk`)
        REFERENCES `youtrack`.`vehicle` (`vehicle_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- ------------------------------------------------------------
-- Table `youtrack`.`gpsreading` -- for GPS readings 
-- ------------------------------------------------------------;
DROP TABLE IF EXISTS `youtrack`.`gpsreading`;

CREATE TABLE IF NOT EXISTS `youtrack`.`gpsreading`(
    `longitude` DECIMAL(10,5),
    `latitude` DECIMAL(10,5),
    `speed` DECIMAL(10,5),
    `vehicle_id_fk` INT(70) NULL,
    `timestamp` DATETIME NOT NULL,
    CONSTRAINT `gps_vehicle_foreign_key`
        FOREIGN KEY (`vehicle_id_fk`)
        REFERENCES `youtrack`.`vehicle` (`vehicle_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
ENGINE = InnoDB;
