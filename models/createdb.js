var mysql      = require('mysql');
var fs         = require('fs');
var config     = require('../config');

var connection = mysql.createConnection({
    host               : config['dbHostProd'],
    port               : config['dbPortProd'],
    user               : config['dbUserProd'],
    password           : config['dbPasswordProd'],
    multipleStatements : true
});

connection.connect();


fs.readFile(__dirname + '/db.sql', function(err, data){
    if(err) console.trace(err);
    var queries = data.toString().trim();
    connection.query(queries, function(error, results){
        if(error) throw error;
        connection.end();
    });
});
