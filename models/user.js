var DB        = require('./db').DB;
//var Promise = require('bluebird');
//var bcrypt  = Promise.promisifyAll(require('bcrypt-nodejs'));
var bcrypt    = require('bcrypt-nodejs');
var jwt       = require('jsonwebtoken');
var config    = require('../config');
var TrackBox  = require('./trackbox');
var Vehicle   = require('./vehicle');

var User = DB.Model.extend({
    tableName: 'user_info',
    idAttribute: 'user_id',
    trackboxes: function(){
        return this.hasMany(TrackBox, 'user_id_fk');
    },
    vehicles: function(){
        return this.hasMany(Vehicle, 'user_id_fk');
    },
    initialize: function(){
        this.on('saving', this.validateSave);
    },
    validateSave: function(){
        //todo add validations
    },
    generateJWT: function(){
        //token expires in 7 days
        return jwt.sign({email: this.attributes.user_email}
                        , config.secret, {expiresIn: '7d'} );
    },
    },{
    createPassword: function(password, cb){
       bcrypt.genSalt(10, function(err, salt){
           if(err) return reject(err);
           bcrypt.hash(password, salt,null , function(err, hash){
               if(err) return err;
               cb(hash);
           });
        });
    }
    }
);

module.exports = User;
