var DB       = require('./db').DB;
var User     = require('./user');
var Vehicle  = require('./vehicle');

var Gpsreading = DB.Model.extend({
    //Instance properties
    tableName: 'gpsreading',
    vehicle: function(){
        return this.belongsTo(Vehicle, 'vehicle_id_fk');
    }
},
{
    //Class properties
});

module.exports = Gpsreading;
