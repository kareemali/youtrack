var DB = require('./db').DB;
var User = require('./user');
var Vehicle = require('./vehicle');

var TrackBox = DB.Model.extend({
    //Instance properties
    tableName: 'trackbox',
    idAttribute: 'trackbox_id',
    user: function(){
        return this.belongsTo(User, 'user_id_fk');
    },
    vehicle: function(){
        return this.hasOne(Vehicle, 'trackbox_id_fk');
    }
},
{
    //Class properties
});

module.exports = TrackBox;
