var config = require('../config');


//drop and create the db from db.sql file
//require('./createdb');
//var knex = require('knex')({
    //client: 'mysql',
    //connection: {
        //host: config['dbHostProd'],
        //user: config['dbUserProd'],
        //port: config['dbPortProd'],
        //password: config['dbPasswordProd'],
        //database: 'youtrack',
        //charset: 'utf8'
    //},
    ////debug: true
//});

var knex = require('knex')({
    client: 'mysql',
    connection: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'youtrack',
        charset: 'utf8'
    },
    //debug: true
});

var bookshelf = require('bookshelf')(knex);

module.exports.DB = bookshelf;
module.exports.knex = knex;
