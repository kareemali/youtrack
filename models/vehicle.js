var DB          = require('./db').DB;
var User        = require('./user');
var Trackbox    = require('./trackbox');
var Accreading  = require('./accreading');
var Gpsreading  = require('./gpsreading');
var Gyroreading = require('./gyroreading');

var Vehicle = DB.Model.extend({
    //Instance properties
    tableName: 'vehicle',
    idAttribute: 'vehicle_id',
    user: function(){
        return this.belongsTo(User, 'user_id_fk');
    },
    trackbox: function(){
        return this.belongsTo(Trackbox, 'trackbox_id_fk');
    },
    accreadings: function(){
        return this.hasMany(Accreading, 'vehicle_id_fk');
    },
    gyroreadings: function(){
        return this.hasMany(Gyroreading, 'vehicle_id_fk');
    },
    gpsreadings: function(){
        return this.hasMany(Gpsreading, 'vehicle_id_fk');
    }
},
{
    //Class properties
});

module.exports = Vehicle;
